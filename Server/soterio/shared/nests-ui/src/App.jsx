import React, { Component } from 'react';
import { ThemeProvider, CSSReset, Box } from '@chakra-ui/core'

import { Button } from '@chakra-ui/core'
import CalendarComponent from './calendar/Calendar';
import { theme } from "@chakra-ui/core";
import DefaultNavbar from './navbar/Navbar';

theme.fonts.body = "Inter, sans-serif"


class App extends Component {
    state = {  }
    render() { 
        return ( 
                <ThemeProvider theme={theme}>
                    <CSSReset/>
                                <Box className="container mx-auto">
                                    <div className="block">
                                        <p className="text-6xl font-bold">The beginning of Nests-UI</p>
                                        <p>A UI library for Soterio Web Applications by Team Guava</p>
                                        <p className="my-4">
                                            <Button variantColor="green">Get started</Button>
                                        </p>
                                    </div>
                                    <div className="mt-12 mb-4">
                                        <p className="text-2xl font-bold">Components</p>
                                    </div>
                                    <div className="my-12">
                                        <p className="text-xl my-6">Default Navbar</p>
                                        <DefaultNavbar />
                                    </div>
                                </Box>
                </ThemeProvider>
         );
    }
}
 
export default App;