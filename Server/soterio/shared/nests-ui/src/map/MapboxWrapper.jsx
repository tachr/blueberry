import React from 'react';

var MapboxWrapper = function(MapComponent) {
    return class Wrapper extends React.Component {

        constructor() {
            super();
            this.state = {
                token: 'pk.eyJ1IjoicHV0aW56dyIsImEiOiJjanp2c2FoNnEwM2cyM2pwbjN0bDJmbHF6In0.kLrpWMUaOZVhsJwqBWcumg',

            };
        }
    
        render() {
            return (
                <div>
                    <MapComponent map></MapComponent>
                </div>
            )
        }
    }
}

export default MapboxWrapper