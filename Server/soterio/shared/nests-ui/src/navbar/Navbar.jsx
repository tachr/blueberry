import React, { Component } from 'react';
import { Box, Button } from '@chakra-ui/core';
import NotificationDirectIcon from '@atlaskit/icon/glyph/notification-direct';
import UserAvatarCircleIcon from '@atlaskit/icon/glyph/user-avatar-circle';
import InfoIcon from '@atlaskit/icon/glyph/info';

class DefaultNavbar extends Component {
    state = {  }
    render() { 
        return ( 
            <Box className="top-0 left-0 border-b-2 border-gray-400 p-4 lg:flex justify-between">
                <Box className="justify-start">Banner</Box>
                <Box className="justify-end lg:w-1/2">
                    <div className="flex justify-end">
                        <div className="">
                            <Button className="p-2" variant="ghost">
                                <InfoIcon label="Notifications" />
                            </Button>
                        </div>
                        <div className="">
                            <Button className="p-2" variant="ghost">
                                <NotificationDirectIcon label="Notifications" />
                            </Button>
                        </div>
                        <div className="">
                            <Button className="p-2" variant="ghost">
                                <UserAvatarCircleIcon label="Account" />
                            </Button>
                        </div>
                    </div>
                </Box>
            </Box>
         );
    }
}
 
export default DefaultNavbar;