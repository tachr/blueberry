import React, { Component } from 'react';
import Calendar from '@atlaskit/calendar';


class CalendarComponent extends Component {
    state = { 
        calendarState: {
            selectedDate: ""
        }
     }

    getSelectedDate = () => {
        return this.state.calendarState.selectedDate;
    }
    
    render() { 
        return ( 
            <Calendar />
         );
    }
}
 
export default CalendarComponent;