package zw.co.soterio.monitor

import android.content.Context
import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.*
import org.junit.rules.TestRule
import org.junit.runner.RunWith
import zw.co.soterio.monitor.storage.DBInstance
import zw.co.soterio.monitor.storage.dao.EncounterDao
import zw.co.soterio.monitor.storage.entity.Encounter

@RunWith(AndroidJUnit4::class)
class EncounterDaoTest {
    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var database: DBInstance
    private lateinit var encounterDao: EncounterDao

    @Before
    fun setup() {
        val context: Context = getTargetContext()
        try {
            database = Room.inMemoryDatabaseBuilder(context, DBInstance::class.java)
                .allowMainThreadQueries().build()
        } catch (e: Exception) {
            Log.i("test", e.message)
        }
        encounterDao = database.encounterDao()
    }

    @Test
    fun testAddingAndRetrievingData() {
        // 1
        val preInsertRetrievedEncounter = encounterDao.getAllEncounters()

        // 2
        val encounter = Encounter(1, "8387473893", 8882348948, 12.8839299, 30.00948393, 87, "Samsung S6")
        encounterDao.insertEncounters(encounter)

        //3
        val postInsertRetrieveEncounter = encounterDao.getAllEncounters()
        val sizeDifference = postInsertRetrieveEncounter.size - preInsertRetrievedEncounter.size
        Assert.assertEquals(1, sizeDifference)
        val retrievedCategory = postInsertRetrieveEncounter.last()
        Assert.assertEquals("8387473893", retrievedCategory.Identifier)
    }

    @After
    fun tearDown() {
        database.close()
    }
}