package zw.co.soterio.monitor.ble

import android.app.Service
import android.bluetooth.*
import android.content.Context
import android.util.Log
import zw.co.soterio.monitor.Constants
import zw.co.soterio.monitor.MonitorApplication


class GattServer (private val context: Context) {
    private val mBluetoothManager:BluetoothManager = context.getSystemService(Service.BLUETOOTH_SERVICE) as BluetoothManager
    private lateinit var mGattServer: BluetoothGattServer

     fun setupServer() {
         mGattServer = mBluetoothManager.openGattServer(context, gattServerCallback)
         Log.d("GattServer", "GattServer has been launched")
         val service = BluetoothGattService(
             Constants.MONITOR_SERVICE_UUID,
             BluetoothGattService.SERVICE_TYPE_PRIMARY
         )
         val identityCharacteristic = BluetoothGattCharacteristic(
             Constants.MONITOR_IDENTITY_CHARACTERISTIC_UUID,
             BluetoothGattCharacteristic.PROPERTY_READ,
             BluetoothGattCharacteristic.PERMISSION_READ
         )
         identityCharacteristic.value =
             MonitorApplication.appUtils!!.getDeviceIdentifier()!!.toByteArray()
         service.addCharacteristic(identityCharacteristic)
         mGattServer.addService(service)
         Log.d("GattServer", "Launching Gatt Server")
    }

    private val gattServerCallback = object: BluetoothGattServerCallback() {

        override fun onConnectionStateChange(device: BluetoothDevice, status: Int, newState: Int) {
            super.onConnectionStateChange(device, status, newState)
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d("GattServer", "Connected to ${device.address}")
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d("GattServer", "Disconnected")
            }
        }
        override fun onServiceAdded(status: Int, service: BluetoothGattService?) {
            super.onServiceAdded(status, service)
            Log.d("GattServer", "Added service to gatt $service")

        }

        override fun onCharacteristicReadRequest(
            device: BluetoothDevice?,
            requestId: Int,
            offset: Int,
            characteristic: BluetoothGattCharacteristic?
        ) {
            super.onCharacteristicReadRequest(device, requestId, offset, characteristic)
            mGattServer.sendResponse(device, requestId, 0, offset, characteristic!!.value)
        }

    }




}