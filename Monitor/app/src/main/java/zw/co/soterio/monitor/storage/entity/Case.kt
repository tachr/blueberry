package zw.co.soterio.monitor.storage.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Cases")
data class Case (@ColumnInfo(name="id") @PrimaryKey(autoGenerate = true) var id: Long = 0,
                      @ColumnInfo(name="Identifier") var Identifier: String,
                      @ColumnInfo(name="Timestamp") var Timestamp: Long)