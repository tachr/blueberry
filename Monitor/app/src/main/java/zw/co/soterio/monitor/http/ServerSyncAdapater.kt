package zw.co.soterio.monitor.http

import android.content.Context
import android.util.Log
import androidx.room.RoomDatabase
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import com.google.android.material.snackbar.Snackbar
import org.json.JSONArray
import org.json.JSONException
import timber.log.Timber
import zw.co.soterio.monitor.R
import zw.co.soterio.monitor.storage.DBInstance
import zw.co.soterio.monitor.storage.entity.Case
import zw.co.soterio.monitor.storage.entity.GeofenceEntity
import kotlin.math.roundToInt


class ServerSyncAdapater(context: Context) {
    private val mContext = context
    private lateinit var appDatabase: RoomDatabase



    fun syncServerDB() {
        val requestQueue = Volley.newRequestQueue(mContext)

        // Initialize a new JsonArrayRequest instance

        // Initialize a new JsonArrayRequest instance
        val exposureCasesRequest = JsonArrayRequest(Request.Method.GET, mContext.getString(R.string.server_url) + "/api/mobile/covid", null,
            Response.Listener<JSONArray?> { response ->
                // Do something with response
                //mTextView.setText(response.toString());
                val thread2: Thread = object : Thread() {
                    override fun run() {
                        (appDatabase as DBInstance).caseDao().nukeCases()
                    }
                }
                thread2.start()

                try {
                    // Loop through the array elements
                    for (i in 0 until response!!.length()) {
                        // Get current json object
                        val thisCase = response.getJSONObject(i)

                        // Get the current student (json object) data
                        val Identifier = thisCase.getString("Identifier")
                        val Timestamp = thisCase.getString("Timestamp").toLong()
                        val Riskscore = thisCase.getString("Riskscore")

                        val case = Case(0, Identifier, Timestamp)
                        val thread: Thread = object : Thread() {
                            override fun run() {
                                (appDatabase as DBInstance).caseDao().insertCases(case)
                                Log.d("Sync", "Saved Encounter $case")
                            }
                        }
                        thread.start()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { // Do something when error occurred
                Timber.d("An error occured ${it.localizedMessage}")
            }
        )

        val geozonesRequest = JsonArrayRequest(Request.Method.GET, mContext.getString(R.string.server_url) + "/api/mobile/redzones", null,
            Response.Listener<JSONArray?> { response ->
                // Do something with response
                //mTextView.setText(response.toString());

                try {
                    // Loop through the array elements
                    val thread1: Thread = object : Thread() {
                        override fun run() {
                            (appDatabase as DBInstance).geofenceDao().nukeGeofences()
                        }
                    }
                    thread1.start()

                    for (i in 0 until response!!.length()) {
                        // Get current json object
                        val thisCase = response.getJSONObject(i)

                        // Get the current student (json object) data
                        val AreaCode = thisCase.getString("Identifier")
                        val AreaName = thisCase.getString("Timestamp")
                        val Latitude = thisCase.getString("Riskscore").toDouble()
                        val Longitude = thisCase.getString("Riskscore").toDouble()
                        val Radius = thisCase.getString("Riskscore").toFloat()
                        val VectorScore = thisCase.getString("Riskscore").toDouble()


                        val geofence = GeofenceEntity(0, AreaCode, AreaName,Latitude, Longitude,
                            Radius.roundToInt(), VectorScore)
                        val thread: Thread = object : Thread() {
                            override fun run() {
                                (appDatabase as DBInstance).geofenceDao().insertGeofence(geofence)
                                Log.d("Sync", "Saved Geofence $geofence")
                            }
                        }
                        thread.start()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            },
            Response.ErrorListener { // Do something when error occurred
                Timber.d("An error occured ${it.localizedMessage}")
            }
        )

        // Add JsonArrayRequest to the RequestQueue

        // Add JsonArrayRequest to the RequestQueue
        requestQueue.add(exposureCasesRequest)
        requestQueue.add(geozonesRequest)
    }
}