package zw.co.soterio.monitor.receivers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.location.GeofencingEvent
import zw.co.soterio.monitor.MainActivity
import zw.co.soterio.monitor.MonitorApplication
import zw.co.soterio.monitor.R


class GeofenceReceiver: BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val geofencingEvent = GeofencingEvent.fromIntent(intent)
        Log.d(TAG, "GeofenceReceiver invoked")
        if (geofencingEvent.hasError()) {
            val errorMessage = GeofenceStatusCodes.getStatusCodeString(geofencingEvent.errorCode)
            Log.e(TAG, errorMessage)
            return
        }
        val geofenceTransition = geofencingEvent.geofenceTransition
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER || geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            val triggeringGeofences = geofencingEvent.triggeringGeofences

            // Get the transition details as a String.
            val geofenceTransitionDetails = getGeofenceTransitionDetails(
                this,
                geofenceTransition,
                triggeringGeofences
            )

            // Send notification and log the transition details.

            sendNotification(geofenceTransitionDetails, context)
            Log.i(TAG, geofenceTransitionDetails)
        } else {
            // Log the error.
            Log.d(TAG, "INVALID_TRANSITION_TYPE")
        }

    }

    private fun getGeofenceTransitionDetails(geofenceReceiver: GeofenceReceiver, geofenceTransition: Int, triggeringGeofences: List<Geofence>): String {
        // get the ID of each geofence triggered
        val triggeringGeofencesList: ArrayList<String?> = ArrayList()
        for (geofence in triggeringGeofences) {
            triggeringGeofencesList.add(geofence.requestId)
        }

        var status: String? = null
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) status =
            "You have entered a COVID-19 Red Zone - " else if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) status = "You have left the COVID-19 Red Zone - "
            else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_DWELL) status = " You have stayed too long in a COVID-19 Red Zone"
        return status + TextUtils.join(", ", triggeringGeofencesList)
    }

    private fun sendNotification(geofenceTransitionDetails: String, context: Context?) {

        MonitorApplication.appUtils!!.setGeofenceAlert(geofenceTransitionDetails)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = "com.soterio.geofence_notification"
            val descriptionText = "Geofence Channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel("28377346", name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager =
                context?.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        val builder = context?.let {
            NotificationCompat.Builder(it, "28377346")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setColor(Color.RED)
                .setContentTitle("Red Zone Alert")
                .setContentText(geofenceTransitionDetails)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        }

        with(context?.let { NotificationManagerCompat.from(it) }) {
            builder?.build()?.let { this?.notify(1, it) }
        }

    }


    companion object {
        private const val TAG = "GeofenceReceiver"
    }

}