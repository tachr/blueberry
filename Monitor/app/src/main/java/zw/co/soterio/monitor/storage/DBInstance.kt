package zw.co.soterio.monitor.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import zw.co.soterio.monitor.storage.dao.CaseDao
import zw.co.soterio.monitor.storage.dao.EncounterDao
import zw.co.soterio.monitor.storage.dao.GeofenceDao
import zw.co.soterio.monitor.storage.entity.Case
import zw.co.soterio.monitor.storage.entity.Encounter
import zw.co.soterio.monitor.storage.entity.GeofenceEntity

@Database(entities = [(Encounter::class), (GeofenceEntity::class), (Case::class)], version = 4)
abstract class DBInstance: RoomDatabase() {
    abstract fun encounterDao(): EncounterDao
    abstract fun geofenceDao(): GeofenceDao
    abstract fun caseDao(): CaseDao

}
