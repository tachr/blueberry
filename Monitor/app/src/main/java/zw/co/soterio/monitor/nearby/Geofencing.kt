package zw.co.soterio.monitor.nearby

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.RoomDatabase
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import zw.co.soterio.monitor.MonitorApplication
import zw.co.soterio.monitor.receivers.GeofenceReceiver

class Geofencing(context: Context): LocationListener, GoogleApiClient.ConnectionCallbacks {
    private lateinit var googleApiClient: GoogleApiClient
    private lateinit var geofencingClient: GeofencingClient
    private lateinit var geofencingRequest: GeofencingRequest
    lateinit var localBroadcastManager: LocalBroadcastManager

    private var geofenceList = arrayListOf<Geofence>()
    private var locationRequest: LocationRequest? = null
    private var mContext:Context = context

    public fun startGeofencing() {
        localBroadcastManager = LocalBroadcastManager.getInstance(mContext)
        geofencingClient = LocationServices.getGeofencingClient(mContext)
        addGeofencesToController()
        val intentFilter = IntentFilter("GEOFENCES_UPDATED")
        localBroadcastManager.registerReceiver(geofenceReceiver, intentFilter)
        googleApiClient = GoogleApiClient.Builder(mContext)
            .addApi(LocationServices.API)
            .build()

        googleApiClient.connect()
    }

    private val UPDATE_INTERVAL = 1000
    private val FASTEST_INTERVAL = 900

    // Start location Updates
    private fun startLocationUpdates() {
        Log.i(TAG, "startLocationUpdates()")
        locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL.toLong())
            .setFastestInterval(FASTEST_INTERVAL.toLong())
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
    }

    override fun onLocationChanged(location: Location) {
        Log.d(TAG, "onLocationChanged [$location]")
    }

    private val geofenceReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            addGeofencesToController()
            startGeofencing()
        }
    }


    private fun addGeofencesToController() {
        val appDatabase: RoomDatabase
        appDatabase = MonitorApplication.database!!

        AsyncTask.execute {
            val geofences = appDatabase.geofenceDao().getAllGeofences()
            Log.d(TAG, "$geofences")

            for (it in geofences) {
                geofenceList.add(
                    Geofence.Builder()
                        .setRequestId(it.AreaName)
                        .setCircularRegion(
                            it.Latitude,
                            it.Longitude,
                            it.Radius.toFloat()
                        )
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_DWELL or Geofence.GEOFENCE_TRANSITION_EXIT)
                        .setLoiteringDelay(600000)
                        .build())
            }

            if(geofenceList.isNotEmpty()) {
                geofencingRequest = GeofencingRequest.Builder().apply {
                    setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                    addGeofences(geofenceList)
                }.build()
            }

            if(geofenceList.isNotEmpty()) {
                geofencingClient.addGeofences(geofencingRequest, geofencePendingIntent)?.run {
                    addOnSuccessListener {
                        Log.d(TAG, "Geofences Added");
                    }
                    addOnFailureListener {
                        // Failed to add geofences
                        // ...
                    }
                }
            }

        }

    }


    private val geofencePendingIntent: PendingIntent by lazy {
        val intent = Intent(context, GeofenceReceiver::class.java)
        PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    companion object {
        private const val TAG = "GeofencingService"
    }

    override fun onConnected(p0: Bundle?) {
        startLocationUpdates()
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d(TAG, "Connection Suspended")
    }

    fun restartService() {
    }
}