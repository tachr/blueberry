package zw.co.soterio.monitor.storage.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Geofences")
data class GeofenceEntity (@ColumnInfo(name="id") @PrimaryKey(autoGenerate = true) var id: Long = 0,
                           @ColumnInfo(name="AreaCode") var AreaCode: String,
                           @ColumnInfo(name="AreaName") var AreaName: String,
                           @ColumnInfo(name="Latitude") var Latitude: Double,
                           @ColumnInfo(name="Longitude") var Longitude: Double,
                           @ColumnInfo(name="Radius") var Radius: Int,
                           @ColumnInfo(name="VectorScore") var VectorScore: Double)