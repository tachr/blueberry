package zw.co.soterio.monitor

import android.app.ActivityOptions
import android.content.Intent
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import org.json.JSONObject
import zw.co.soterio.monitor.storage.User
import zw.co.soterio.monitor.utils.CryptoUtils

class MobilePhoneActivity : AppCompatActivity() {

    private lateinit var backBtn: MaterialButton
    private lateinit var continueBtn: MaterialButton
    private lateinit var primaryPhoneNumber: TextInputLayout
    private lateinit var secondaryPhoneNumber: TextInputLayout
    private lateinit var nextOfKinPhoneNumber: TextInputLayout
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mobile_phone)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(applicationContext)

        primaryPhoneNumber = findViewById(R.id.primaryPhone)
        secondaryPhoneNumber = findViewById(R.id.secondaryPhone)
        nextOfKinPhoneNumber = findViewById(R.id.nextOfKinPhone)

        backBtn = findViewById(R.id.btnBack)
        continueBtn = findViewById(R.id.btnContinuePhone)

        backBtn.setOnClickListener{
            onBackPressed()
        }

        continueBtn.setOnClickListener {
            Log.d("Continue", "Continue Clicked")
            when {
                primaryPhoneNumber.editText!!.text.toString() == "" -> {
                    primaryPhoneNumber.error = "Primary phone number is required"
                }
                else -> {
                    MonitorApplication.appUtils!!.setPhoneNumber(primaryPhoneNumber!!.editText!!.text.toString())
                    MonitorApplication.appUtils!!.setShouldOnBoard()
                    CryptoUtils(applicationContext).encryptPhone(primaryPhoneNumber.editText!!.text.toString())

                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location : Location? ->
                            if(location !=null) {
                                Log.d("Location", "Has location")
                                val user = User(
                                    MonitorApplication.appUtils!!.getUserFirstName()!!,
                                    MonitorApplication.appUtils!!.getUserSurname()!!,
                                    MonitorApplication.appUtils!!.getDateOfBirth()!!,
                                    MonitorApplication.appUtils!!.getDeviceIdentifier()!!,
                                    MonitorApplication.appUtils!!.getUserAddress()!!,
                                    primaryPhoneNumber.editText!!.text.toString(),
                                    secondaryPhoneNumber.editText!!.text.toString(),
                                    nextOfKinPhoneNumber.editText!!.text.toString(),
                                    location.latitude,
                                    location.longitude
                                )

                                val mRequestQueue = Volley.newRequestQueue(applicationContext)

                                //String Request initialized
                                val mStringRequest = object : StringRequest(Request.Method.POST, "http://10.80.3.194:7000/api/mobile/new", Response.Listener { _ ->
                                    MonitorApplication.appUtils!!.setEncounterLastSync(System.currentTimeMillis())
                                    MonitorApplication.appUtils!!.setUserRegistered(true)


                                }, Response.ErrorListener { error ->
                                    MonitorApplication.appUtils!!.setUserRegistered(true)
                                }) {
                                    override fun getBodyContentType(): String {
                                        return "application/json"
                                    }

                                    @Throws(AuthFailureError::class)
                                    override fun getBody(): ByteArray {
                                        val params2 = HashMap<String, String>()
                                        val gson = Gson()
                                        val packetJSON = gson.toJson(user)
                                        params2["User"] = packetJSON
                                        return JSONObject(packetJSON).toString().toByteArray()
                                    }
                                }
                                mRequestQueue.add(mStringRequest)
                                mRequestQueue.start()

                            } else {
                                Log.d("Location", "Has no location")

                                val user = User(
                                    MonitorApplication.appUtils!!.getUserFirstName()!!,
                                    MonitorApplication.appUtils!!.getUserSurname()!!,
                                    MonitorApplication.appUtils!!.getDateOfBirth()!!,
                                    MonitorApplication.appUtils!!.getDeviceIdentifier()!!,
                                    MonitorApplication.appUtils!!.getUserAddress()!!,
                                    primaryPhoneNumber.editText!!.text.toString(),
                                    secondaryPhoneNumber.editText!!.text.toString(),
                                    nextOfKinPhoneNumber.editText!!.text.toString(),
                                    0.000,
                                    0.000
                                )
                                Log.d("Location", "User is $user")

                                val mRequestQueue = Volley.newRequestQueue(applicationContext)

                                //String Request initialized
                                val mStringRequest = object : StringRequest(Request.Method.POST, "http://10.80.3.194:7000/api/mobile/new", Response.Listener { _ ->
                                    MonitorApplication.appUtils!!.setEncounterLastSync(System.currentTimeMillis())
                                    MonitorApplication.appUtils!!.setUserRegistered(true)

                                }, Response.ErrorListener { error ->
                                    Log.i("This is the error", "Error :" + error.toString())
                                }) {
                                    override fun getBodyContentType(): String {
                                        return "application/json"
                                    }

                                    @Throws(AuthFailureError::class)
                                    override fun getBody(): ByteArray {
                                        val params2 = HashMap<String, String>()
                                        val gson = Gson()
                                        val packetJSON = gson.toJson(user)
                                        params2["User"] = packetJSON
                                        return JSONObject(params2 as Map<*, *>).toString().toByteArray()
                                    }
                                }
                                mRequestQueue.add(mStringRequest)
                                mRequestQueue.start()

                            }
                        }
                        .addOnFailureListener {
                            Log.d("Location", "Has errored location")

                            val user = User(
                                MonitorApplication.appUtils!!.getUserFirstName()!!,
                                MonitorApplication.appUtils!!.getUserSurname()!!,
                                MonitorApplication.appUtils!!.getDateOfBirth()!!,
                                MonitorApplication.appUtils!!.getDeviceIdentifier()!!,
                                MonitorApplication.appUtils!!.getUserAddress()!!,
                                primaryPhoneNumber.editText!!.text.toString(),
                                secondaryPhoneNumber.editText!!.text.toString(),
                                nextOfKinPhoneNumber.editText!!.text.toString(),
                                0.000,
                                0.000
                            )
                            Log.d("Location", "$user")


                            val mRequestQueue = Volley.newRequestQueue(applicationContext)

                            //String Request initialized
                            val mStringRequest = object : StringRequest(Request.Method.POST, "http://10.80.3.194:7000/api/mobile/new", Response.Listener { _ ->
                                MonitorApplication.appUtils!!.setEncounterLastSync(System.currentTimeMillis())


                            }, Response.ErrorListener { error ->
                                Log.i("This is the error", "Error :" + error.toString())
                            }) {
                                override fun getBodyContentType(): String {
                                    return "application/json"
                                }

                                @Throws(AuthFailureError::class)
                                override fun getBody(): ByteArray {
                                    val params2 = HashMap<String, String>()
                                    val gson = Gson()
                                    val packetJSON = gson.toJson(user)
                                    params2["User"] = packetJSON
                                    return JSONObject(packetJSON).toString().toByteArray()
                                }
                            }
                            mRequestQueue.add(mStringRequest)
                            mRequestQueue.start()

                        }
                    startActivity(Intent(this, MainActivity::class.java), ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
                    finish()
                    Log.d("Splash", "Identifier set to ${MonitorApplication.appUtils!!.getDeviceIdentifier()}")

                }
            }
        }
    }
}
