package zw.co.soterio.monitor

import android.Manifest
import android.annotation.TargetApi
import android.bluetooth.BluetoothAdapter
import android.content.*
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.RoomDatabase
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest.PRIORITY_LOW_POWER
import com.google.android.gms.location.LocationRequest.create
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.InternalCoroutinesApi
import org.json.JSONException
import org.json.JSONObject
import timber.log.Timber
import zw.co.soterio.monitor.services.ApplicationForegroundService
import zw.co.soterio.monitor.storage.DBInstance
import zw.co.soterio.monitor.storage.User
import zw.co.soterio.monitor.storage.entity.Case
import zw.co.soterio.monitor.storage.entity.GeofenceEntity

open class MainActivity : AppCompatActivity() {
    private val TAG = "MainActivity"
    val MY_PERMISSIONS_REQUEST_LOCATION = 99
    private lateinit var appDatabase:RoomDatabase
    private val runningQOrLater = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q
    lateinit var localBroadcastManager: LocalBroadcastManager

    private lateinit var geoNotiText: TextView
    private lateinit var notificationCounter: TextView

    private lateinit var mExposureAlert: RelativeLayout
    private lateinit var mGeofenceAlert: RelativeLayout
    private lateinit var mDismissGeofenceButton: MaterialButton


    private var counter = 0

    @InternalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable ->
            Log.e(
                "Error" + Thread.currentThread().stackTrace[2],
                paramThrowable.localizedMessage
            )
        }
        localBroadcastManager = LocalBroadcastManager.getInstance(applicationContext)

        checkForHardwareSupport()
        checkPermissions()
        initializeRefs()
        val intentFilter = IntentFilter("CASES_UPDATED")
        localBroadcastManager.registerReceiver(casesReceiver, intentFilter)

        /*
        val t = Timer()
        t.scheduleAtFixedRate(
            object : TimerTask() {
                override fun run() {
                    getUpdateFromServer()
                }
            },  //Set how long before to start calling the TimerTask (in milliseconds)
            0,  //Set the amount of time between each execution (in milliseconds)
            30000
        )
        */

    }

    private fun initializeRefs() {
        appDatabase = MonitorApplication.database!!
        MonitorApplication.appUtils!!.setGeofenceAlert("You have entered a COVID-19 Red Zone - CUT")
        mExposureAlert = findViewById(R.id.exposureAlert)
        mGeofenceAlert = findViewById(R.id.geofenceAlert)
        geoNotiText = findViewById(R.id.geoNotiText)
        mDismissGeofenceButton = findViewById(R.id.geofenceDismiss)
        notificationCounter = findViewById(R.id.notificationCounter)

        //
        mExposureAlert.visibility = View.GONE
        mGeofenceAlert.visibility = View.GONE
        if(MonitorApplication.appUtils!!.getExposureStatus()!!) {
            showExposureAlert()

        }

        if(MonitorApplication.appUtils!!.getGeofenceAlert()!! != "nonce") {
            showGeofenceAlert()

        }

        mDismissGeofenceButton.setOnClickListener {
            hideGeofenceAlert()
            MonitorApplication.appUtils!!.setGeofenceAlert("nonce")
        }

        showGeofenceAlert()
    }

    private fun showGeofenceAlert() {
        counter++
        mGeofenceAlert.visibility = View.VISIBLE
        geoNotiText.text = MonitorApplication.appUtils!!.getGeofenceAlert()
        notificationCounter.text = "$counter Exposure notifications"

    }

    private fun showExposureAlert() {
        counter++
        mExposureAlert.visibility = View.VISIBLE
        notificationCounter.text = "$counter Exposure notifications"

    }

    private fun hideGeofenceAlert() {
        counter--
        mGeofenceAlert.visibility = View.GONE
        notificationCounter.text = "$counter Exposure notifications"
        MonitorApplication.appUtils!!.setGeofenceAlert("nonce")

    }
    private fun hideExposureAlert() {
        counter--
        mExposureAlert.visibility = View.GONE
        notificationCounter.text = "$counter Exposure notifications"

    }















    private fun checkForHardwareSupport() {
        // Check if device supports bluetooth le... if not close the app.. I guess we can show the user a message here if its not supported
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Device has no support for BLE. Exiting", Toast.LENGTH_LONG).show()
        }
        // Check if device supports bluetooth multiple advertisement... if not close the app.. I guess we can show the user a message here if its not supported

        if(!BluetoothAdapter.getDefaultAdapter().isMultipleAdvertisementSupported) {
            Timber.d("Multiple Advertisement not supported")
        }
    }

    /********************************
     *
     * PERMISSIONS AND STUFF
     *
     *******************************/

    private fun checkPermissions() {
        if(runningQOrLater) {
            if (foregroundAndBackgroundLocationPermissionApproved()) {
                checkDeviceLocationSettingsAndStartGeofence()

            } else {
                Timber.d("Requesting")
                if(runningQOrLater) {
                    requestForegroundAndBackgroundLocationPermissions()
                }
            }
        } else {
            Log.d("Permissions", "Requesting permissions for lower level")
            if(checkLocationPermission()) {
                startAllServices()
            }

        }

    }

    private fun checkLocationPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)
            ) {
                ActivityCompat.requestPermissions(
                    this@MainActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
            false
        } else {
            true
        }
    }

    @TargetApi(29 )
    private fun requestForegroundAndBackgroundLocationPermissions() {
        if (foregroundAndBackgroundLocationPermissionApproved()){
            return
        }
        var permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
        permissionsArray += Manifest.permission.ACCESS_BACKGROUND_LOCATION
        ActivityCompat.requestPermissions(
            this,
            permissionsArray,
            REQUEST_FOREGROUND_AND_BACKGROUND_PERMISSION_RESULT_CODE
        )
    }

    @TargetApi(29 )
    private fun foregroundAndBackgroundLocationPermissionApproved(): Boolean {
        val foregroundLocationApproved = (
                PackageManager.PERMISSION_GRANTED ==
                        ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION))
        val backgroundPermissionApproved =
            if (runningQOrLater) {
                PackageManager.PERMISSION_GRANTED ==
                        ActivityCompat.checkSelfPermission(
                            this, Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        )
            } else {
                true
            }
        return if (runningQOrLater) {
            foregroundLocationApproved && backgroundPermissionApproved
        } else {
            backgroundPermissionApproved
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        Timber.d("onRequestPermissionResult")

        if (grantResults.isEmpty() || grantResults[LOCATION_PERMISSION_INDEX] == PackageManager.PERMISSION_DENIED || (requestCode == REQUEST_FOREGROUND_AND_BACKGROUND_PERMISSION_RESULT_CODE &&
                    grantResults[BACKGROUND_LOCATION_PERMISSION_INDEX] == PackageManager.PERMISSION_DENIED)
        ) {
            Snackbar.make(
                findViewById(R.id.mainView),
                R.string.permission_denied_explanation,
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(R.string.settings) {
                    startActivity(Intent().apply {
                        action = ACTION_APPLICATION_DETAILS_SETTINGS
                        data = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    })
                }.show()
        } else {
            checkDeviceLocationSettingsAndStartGeofence()
        }
    }

    private fun checkDeviceLocationSettingsAndStartGeofence(resolve:Boolean = true) {
        val locationRequest = create().apply {
            priority = PRIORITY_LOW_POWER
        }
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val settingsClient = LocationServices.getSettingsClient(this)
        val locationSettingsResponseTask =
            settingsClient.checkLocationSettings(builder.build())
        locationSettingsResponseTask.addOnFailureListener { exception ->
            if (exception is ResolvableApiException && resolve){
                try {
                    exception.startResolutionForResult(this@MainActivity,
                        Companion.REQUEST_TURN_DEVICE_LOCATION_ON
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    Log.d(TAG, "Error getting location settings resolution: " + sendEx.message)
                }
            } else {
                Snackbar.make(
                    findViewById(R.id.mainView),
                    R.string.location_required_error, Snackbar.LENGTH_INDEFINITE
                ).setAction(android.R.string.ok) {
                    checkDeviceLocationSettingsAndStartGeofence()
                }.show()
            }
        }
        locationSettingsResponseTask.addOnCompleteListener {
            if ( it.isSuccessful ) {
                Log.d("Service", "Starting Services")
                startAllServices()
            }
        }
    }

    private fun startAllServices() {
        Log.d("Service", "Starting Services")
        startService(Intent(this, ApplicationForegroundService::class.java))
    }


    /********************************
     *
     * ON RESUME
     *
     *******************************/

    private val casesReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            // TODO - Check if there's a positive match
            val thread: Thread = object : Thread() {
                override fun run() {
                    val people  = (appDatabase as DBInstance).caseDao().getAllCases()
                    Log.d("Infection", "Got cases")
                    for (p in people) {
                        if((appDatabase as DBInstance).encounterDao().getEncountersByIdentifier(p.Identifier).isNotEmpty()){
                            val localIntent = Intent("POSITIVE_MATCH")
                            showExposureAlert()
                            MonitorApplication.appUtils!!.setExposureStatus(true)
                            localBroadcastManager.sendBroadcast(localIntent)
                            Log.d("Infection", "${p.Identifier}")
                        }
                    }
                }
            }
            thread.start()
            startAllServices()
        }

    }


    override fun onResume() {
        super.onResume()


        Log.d(TAG, "Has resumed")
        getUpdateFromServer()
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    private fun getUpdateFromServer() {
        if(isNetworkAvailable((this))) {

            if(MonitorApplication.appUtils!!.getEncounterLastSync()!! < (System.currentTimeMillis() - 43000) ) {
                Log.d("Last Updated", "Last updated ${MonitorApplication.appUtils!!.getEncounterLastSync()!!} and now is ${(System.currentTimeMillis() - 43200)}")
                val thread: Thread = object : Thread() {
                    override fun run() {
                        val mRequestQueue = Volley.newRequestQueue(applicationContext)

                        val user = User(
                            MonitorApplication.appUtils!!.getUserFirstName()!!,
                            MonitorApplication.appUtils!!.getUserSurname()!!,
                            MonitorApplication.appUtils!!.getDateOfBirth()!!,
                            MonitorApplication.appUtils!!.getDeviceIdentifier()!!,
                            MonitorApplication.appUtils!!.getUserAddress()!!,
                            MonitorApplication.appUtils!!.getPhoneNumber()!!,
                            "",
                            "",
                            0.0000,
                            0.0000
                        )


                        //String Request initialized
                        val mStringRequest1 = object : StringRequest(Request.Method.POST, "http://10.80.3.194:7000/api/mobile/new", Response.Listener { _ ->
                            MonitorApplication.appUtils!!.setEncounterLastSync(System.currentTimeMillis())
                            MonitorApplication.appUtils!!.setUserRegistered(true)

                        }, Response.ErrorListener { error ->
                            MonitorApplication.appUtils!!.setUserRegistered(true)
                        }) {
                            override fun getBodyContentType(): String {
                                return "application/json"
                            }

                            @Throws(AuthFailureError::class)
                            override fun getBody(): ByteArray {
                                val params2 = HashMap<String, String>()
                                val gson = Gson()
                                val packetJSON = gson.toJson(user)
                                params2["User"] = packetJSON
                                return JSONObject(packetJSON).toString().toByteArray()
                            }
                        }

                        if(!MonitorApplication.appUtils!!.getUserRegistered()!!) {
                            mRequestQueue.add(mStringRequest1)
                        }

                        //String Request initialized
                        val mStringRequest = object : StringRequest(Request.Method.POST, "http://10.80.3.194:7000/api/mobile/log", Response.Listener { _ ->
                            MonitorApplication.appUtils!!.setEncounterLastSync(System.currentTimeMillis())

                        }, Response.ErrorListener { error ->
                            Log.i("This is the error", "Error :" + error.toString())
                        }) {
                            override fun getBodyContentType(): String {
                                return "application/json"
                            }

                            @Throws(AuthFailureError::class)
                            override fun getBody(): ByteArray {
                                val params2 = HashMap<String, String>()
                                params2["Identifier"] = MonitorApplication.appUtils!!.getDeviceIdentifier().toString()
                                val gson = Gson()
                                val packetJSON = gson.toJson((appDatabase as DBInstance).encounterDao().getAllEncounters())
                                params2["Log"] = packetJSON
                                return JSONObject(params2 as Map<*, *>).toString().toByteArray()
                            }

                        }


                        val exposureRequest = StringRequest(Request.Method.GET, "http://10.80.3.194:7000/api/mobile/covid",
                            Response.Listener {
                                try {
                                    val jsonObject = JSONObject(it)
                                    Log.d("Sync", "${jsonObject.getJSONArray("docs")}")
                                    val people: List<Case> = Gson().fromJson(jsonObject.getJSONArray("docs").toString(), Array<Case>::class.java).toList()

                                    val thread: Thread = object : Thread() {
                                        override fun run() {
                                            (appDatabase as DBInstance).caseDao().nukeCases()
                                            for (p in people) {
                                                (appDatabase as DBInstance).caseDao().insertCases(p)
                                                Log.d("Update", "${p.Identifier}")
                                            }
                                            val localIntent = Intent("CASES_UPDATED")
                                            localBroadcastManager.sendBroadcast(localIntent)
                                        }
                                    }
                                    thread.start()
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            },
                            Response.ErrorListener {
                                Log.i("This is the error", "Error :" + it.toString())
                            })

                        val redzoneRequest = StringRequest(Request.Method.GET, "http://10.80.3.194:7000/api/mobile/redzones", Response.Listener {
                            try {
                                val jsonObject = JSONObject(it)
                                Log.d("Sync", "${jsonObject.getJSONArray("docs")}")
                                val people: List<GeofenceEntity> = Gson().fromJson(jsonObject.getJSONArray("docs").toString(), Array<GeofenceEntity>::class.java).toList()

                                val thread: Thread = object : Thread() {
                                    override fun run() {
                                        (appDatabase as DBInstance).geofenceDao().nukeGeofences()
                                        for (p in people) {
                                            (appDatabase as DBInstance).geofenceDao().insertGeofence(p)
                                            val localIntent = Intent("GEOFENCES_UPDATED")
                                            localBroadcastManager.sendBroadcast(localIntent)
                                        }
                                    }
                                }
                                thread.start()
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }

                        }, Response.ErrorListener { error ->
                            Log.i("This is the error", "Error :" + error.toString())
                        })


                        mRequestQueue!!.add(mStringRequest)
                        mRequestQueue.add(exposureRequest)
                        mRequestQueue.add(redzoneRequest)
                    }
                }
                thread.start()
            }


        }
    }


    companion object {
        private const val REQUEST_TURN_DEVICE_LOCATION_ON = 29
        private const val LOCATION_PERMISSION_INDEX = 0
        private const val BACKGROUND_LOCATION_PERMISSION_INDEX = 1
        private const val REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE = 34
        private const val REQUEST_FOREGROUND_AND_BACKGROUND_PERMISSION_RESULT_CODE = 33
    }
}
