package zw.co.soterio.monitor

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.room.Room
import zw.co.soterio.monitor.http.ServerSyncAdapater
import zw.co.soterio.monitor.storage.DBInstance
import zw.co.soterio.monitor.utils.AppUtils
import zw.co.soterio.monitor.utils.CryptoUtils


class MonitorApplication: Application() {

    companion object {
        var database: DBInstance? = null
        var appUtils: AppUtils? = null
    }

    public fun appUtilities(): AppUtils {
        return AppUtils(applicationContext)
    }
    lateinit var localBroadcastManager: LocalBroadcastManager;

    override fun onCreate() {
        super.onCreate()

        Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable ->
            Log.e(
                "Error" + Thread.currentThread().stackTrace[2],
                paramThrowable.localizedMessage
            )
        }
        localBroadcastManager = LocalBroadcastManager.getInstance(applicationContext)
        val positiveFiler = IntentFilter("POSITIVE_MATCH")
        localBroadcastManager.registerReceiver(positiveMatchReceiver, positiveFiler)
        appUtils = AppUtils(applicationContext)
        database = Room.databaseBuilder(this, DBInstance::class.java, "local_master")
            .fallbackToDestructiveMigration()
            .build()

        if(appUtils!!.getDeviceIdentifier() == "nonce") {
            val cryptoUtils = CryptoUtils(applicationContext)
            cryptoUtils.encryptPhone(System.currentTimeMillis().toString())
            appUtils!!.writeDatabaseLastUpdated(System.currentTimeMillis())
        }

        val serverSyncAdapter = ServerSyncAdapater(applicationContext)
        serverSyncAdapter.syncServerDB()

    }

    private val positiveMatchReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val thread: Thread = object : Thread() {
                override fun run() {
                    val cases = database!!.caseDao().getAllCases()
                    for (c in cases) {
                        if(database!!.encounterDao().getEncountersByIdentifier(c.Identifier).isNotEmpty()) {
                            Log.d("Receiver", "You might have been exposed")
                            showPositiveNoti(applicationContext)
                        } else {
                            //Log.d("Sync", "Found ${database!!.encounterDao().getAllEncounters()[0]}")
                        }
                    }
                }
            }
            thread.start()

        }

    }

    private fun showPositiveNoti(context: Context?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = "com.soterio.geofence_notification"
            val descriptionText = "Geofence Channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel("2876733973", name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        val builder = context?.let {
            NotificationCompat.Builder(it, "2876733973")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setColor(Color.RED)
                .setContentTitle("Exposure Alert")
                .setContentText("You might have been exposed to COVID-19")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        }

        with(context?.let { NotificationManagerCompat.from(it) }) {
            builder?.build()?.let { this?.notify(1, it) }
        }



    }

}