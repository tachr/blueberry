package zw.co.soterio.monitor.ble

import android.bluetooth.*
import android.bluetooth.le.*
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import com.polidea.rxandroidble2.RxBleClient
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.*
import timber.log.Timber
import zw.co.soterio.monitor.Constants
import java.util.function.BiFunction


class BluetoothScanner(val context: Context) {
    private val mBluetoothAdapter:BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    private val mBluetoothScanner: BluetoothLeScanner = mBluetoothAdapter.bluetoothLeScanner

    public fun initializeAndStartScan() {
        if(!isBluetoothEnabled()) {
            mBluetoothAdapter.enable()
            startScan()
            Timber.d("Scan starting")
        } else {
            mBluetoothAdapter.enable()
            startScan()
        }
    }

    private fun isBluetoothEnabled(): Boolean {
        return !mBluetoothAdapter.isEnabled
    }

    private fun startScan() {
        val scanSettings: ScanSettings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
                .build()
        val scanFilter: ScanFilter = ScanFilter.Builder()
            .setServiceUuid(ParcelUuid(Constants.MONITOR_SERVICE_UUID))
            .build()
        val filters = ArrayList<ScanFilter>()
        filters.add(scanFilter)
        mBluetoothScanner.startScan(filters, scanSettings, mScanCallback)
    }

    fun stopScan() {

    }

    private var mScanCallback: ScanCallback? = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            if (result?.device == null){
                return
            }
            Log.d("Device","Found device ${result.device.address} with rssi ${result.rssi}")
            result.device.connectGatt(context, false, gattClientCallback)

        }

        override fun onBatchScanResults(results: MutableList<ScanResult>?) {
            super.onBatchScanResults(results)

        }

        override fun onScanFailed(errorCode: Int) {
            Log.d("Device","BLE Discovery onScanFailed: $errorCode")
            super.onScanFailed(errorCode)
        }
    }

    private val gattClientCallback = object: BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (status == BluetoothGatt.GATT_FAILURE) {
                gatt!!.disconnect()
                Log.d("GattClient", "Disconnecting GATT")
                return;
            } else if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("Gatt Client", "Gatt Connected")
                gatt!!.discoverServices()
            }
            Log.d("GattClient", "$status")


            if (newState != BluetoothProfile.STATE_DISCONNECTED) return
                gatt!!.disconnect()
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if (status != BluetoothGatt.GATT_SUCCESS) {
                Log.d("GattClient", "Service Discovery failed with error $status")

                return;
            }
            val service = gatt!!.getService(Constants.MONITOR_SERVICE_UUID)
            val characteristic = service.getCharacteristic(Constants.MONITOR_IDENTITY_CHARACTERISTIC_UUID)
            Log.d("GattClient", "Services discovered ${service.uuid} and characteristic ${characteristic.uuid}")
            gatt.disconnect()
        }
    }



}