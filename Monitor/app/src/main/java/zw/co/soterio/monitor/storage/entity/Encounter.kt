package zw.co.soterio.monitor.storage.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Encounters")
data class Encounter(@ColumnInfo(name="id") @PrimaryKey(autoGenerate = true) var id: Long = 0,
                     @ColumnInfo(name="Identifier") var Identifier: String,
                     @ColumnInfo(name="Timestamp") var Timestamp: Long,
                     @ColumnInfo(name="Latitude") var Latitude: Double,
                     @ColumnInfo(name="Longitude") var Longitude: Double,
                     @ColumnInfo(name="rssi") var rssi: Int,
                     @ColumnInfo(name="Model") var TxPower: Int
)