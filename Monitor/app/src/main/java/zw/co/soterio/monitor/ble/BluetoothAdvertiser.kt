package zw.co.soterio.monitor.ble

import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.bluetooth.le.AdvertiseCallback
import android.bluetooth.le.AdvertiseData
import android.bluetooth.le.AdvertiseSettings
import android.bluetooth.le.BluetoothLeAdvertiser
import android.content.Context
import android.os.ParcelUuid
import android.util.Log
import timber.log.Timber
import zw.co.soterio.monitor.Constants

class BluetoothAdvertiser (private val context: Context) {

    private var mAdvertising: Boolean = false
    private val mBluetoothManager : BluetoothManager = context.getSystemService(Service.BLUETOOTH_SERVICE) as BluetoothManager
    private val mBluetoothAdapter: BluetoothAdapter = mBluetoothManager.adapter
    private val mBluetoothAdvertise: BluetoothLeAdvertiser = mBluetoothAdapter.bluetoothLeAdvertiser


    fun startAdvertiser() {
        Log.d("Advertiser","Starting Advertising")
        val gattServer = GattServer(context)
        gattServer.setupServer()

        val advertisingSettings = AdvertiseSettings.Builder()
            .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_POWER)
            .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_LOW)
            .setConnectable(true)
            .setTimeout(0)
            .build()

        val advertiseData = AdvertiseData.Builder()
            .addServiceUuid(ParcelUuid(Constants.MONITOR_SERVICE_UUID))
            .setIncludeDeviceName(true)
            .setIncludeTxPowerLevel(true)
            .build()

        mBluetoothAdvertise.startAdvertising(advertisingSettings, advertiseData, advertiseCallback)
        mAdvertising = true

    }

    public fun restartAdvertiser() {
        if(mAdvertising) {
            mBluetoothAdvertise.stopAdvertising(advertiseCallback)
            startAdvertiser()
        }
    }

    private val advertiseCallback: AdvertiseCallback = object: AdvertiseCallback() {
        override fun onStartSuccess(settingsInEffect: AdvertiseSettings?) {
            super.onStartSuccess(settingsInEffect)
            Timber.d("Advertising has started with $settingsInEffect")
        }

        override fun onStartFailure(errorCode: Int) {
            super.onStartFailure(errorCode)
            Timber.d("Advertising failed to start. Error Code $errorCode")
        }
    }

}