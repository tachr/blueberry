package zw.co.soterio.monitor.http.models

import zw.co.soterio.monitor.storage.entity.Encounter

class PacketJSON(val Timestamp: Long, encounters: ArrayList<Encounter>) {
}