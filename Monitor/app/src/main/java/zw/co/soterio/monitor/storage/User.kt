package zw.co.soterio.monitor.storage

class User(val FirstName: String,
           val LastName: String,
           val DateOfBirth: String,
           val Identifier: String,
           val PhysicalAddress: String,
           val PhoneNumber: String,
           val SecondaryPhone: String,
           val NextOfKinPhone: String,
           val Latitude: Double,
           val Longitude: Double)  {

}
