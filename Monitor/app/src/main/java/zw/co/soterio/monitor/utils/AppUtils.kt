package zw.co.soterio.monitor.utils

import android.content.Context
import android.content.SharedPreferences
import android.location.Location

class AppUtils(private val context: Context) {
    private val sharedPref: SharedPreferences = context.getSharedPreferences("SoterioUtils", Context.MODE_PRIVATE)

    fun getDeviceIdentifier(): String? {
        return sharedPref.getString("DeviceIdentifier", "nonce")
    }

    fun writeDeviceIdentifier(deviceIdentifier: String) {
        sharedPref.edit().putString("DeviceIdentifier", deviceIdentifier).apply()
    }

    fun getDatabaseLastUpdated(): Long? {
        return sharedPref.getLong("DatabaseLastUpdated", 0)
    }

    fun writeDatabaseLastUpdated(timestamp: Long) {
        sharedPref.edit().putLong("DatabaseLastUpdated", timestamp).apply()
    }

    fun setEncounterLastSync(currentTimeMillis: Long) {
        sharedPref.edit().putLong("EncounterLastSync", currentTimeMillis).apply()

    }

    fun getEncounterLastSync(): Long? {
        return sharedPref.getLong("EncounterLastSync", 0)
    }

    fun setShouldOnBoard() {
        sharedPref.edit().putBoolean("ShouldOnBoard", false).apply()

    }

    fun getShouldOnBoard(): Boolean? {
        return sharedPref.getBoolean("ShouldOnBoard", true)
    }

    fun setUserFirstName(toString: String) {
        sharedPref.edit().putString("FirstName", toString).apply()

    }

    fun setUserSurname(toString: String) {
        sharedPref.edit().putString("Surname", toString).apply()

    }

    fun setUserAddress(toString: String) {
        sharedPref.edit().putString("Address", toString).apply()

    }

    fun setUserBirthday(toString: String) {
        sharedPref.edit().putString("Birthday", toString).apply()

    }


    fun getUserAddress(): String? {
        return sharedPref.getString("Address", "nonce")

    }

    fun getUserSurname(): String? {
        return sharedPref.getString("Surname", "nonce")

    }

    fun getUserFirstName(): String? {
        return sharedPref.getString("FirstName", "nonce")

    }

    fun getDateOfBirth(): String? {
        return sharedPref.getString("Birthday", "01/01/1000")

    }

    fun setExposureStatus(b: Boolean) {
        sharedPref.edit().putBoolean("ExposureStatus", b).apply()

    }

    fun getExposureStatus(): Boolean? {
        return sharedPref.getBoolean("ExposureStatus", false)
    }

    fun getGeofenceAlert(): String? {
        return sharedPref.getString("CurrentGeofence", "nonce")

    }

    fun setGeofenceAlert(toString: String) {
        sharedPref.edit().putString("CurrentGeofence", toString).apply()

    }

    fun setUserRegistered(b: Boolean) {
        sharedPref.edit().putBoolean("UserRegistered", b).apply()
    }


    fun getUserRegistered(): Boolean? {
        return sharedPref.getBoolean("UserRegistered", false)
    }

    fun setPhoneNumber(primaryPhoneNumber: String) {
        sharedPref.edit().putString("PhoneNumber", primaryPhoneNumber).apply()
    }

    fun getPhoneNumber(): String? {
        return sharedPref.getString("PhoneNumber", "nonce")
    }

}