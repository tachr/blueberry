package zw.co.soterio.monitor.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlinx.coroutines.*
import timber.log.Timber
import zw.co.soterio.monitor.R
import zw.co.soterio.monitor.ble.BluetoothAdvertiser
import zw.co.soterio.monitor.ble.RxBleScaner
import zw.co.soterio.monitor.nearby.Geofencing
import zw.co.soterio.monitor.nearby.SyncDeviceAdapter
import zw.co.soterio.monitor.receivers.PusherReceiver
import zw.co.soterio.monitor.utils.AppUtils


class ApplicationForegroundService : Service() {

    private lateinit var geofencingService:Geofencing

    fun defaultDispatcher(): CoroutineDispatcher = Dispatchers.Default
    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        geofencingService.startGeofencing()
        return super.onStartCommand(intent, flags, startId)
    }

    @InternalCoroutinesApi
    override fun onCreate() {
        super.onCreate()
        geofencingService = Geofencing(applicationContext)
        geofencingService.startGeofencing()

        val syncDeviceAdapter = SyncDeviceAdapter(applicationContext)
        syncDeviceAdapter.startAdvertising()
        syncDeviceAdapter.startDiscovery()

        val pusherReceiver = PusherReceiver(applicationContext)
        pusherReceiver.startReceiver()

        // Check if multi-advertisement is supported and start the advertiser
        if(!BluetoothAdapter.getDefaultAdapter().isEnabled) {
            BluetoothAdapter.getDefaultAdapter().enable()
        }

        if(BluetoothAdapter.getDefaultAdapter().isMultipleAdvertisementSupported) {
            val bleAdvertiser = BluetoothAdvertiser(applicationContext)
            bleAdvertiser.startAdvertiser()
            Timber.d("Multi-Advertisement supported. Launching Advertiser")
        }

        val bluetoothScanner = RxBleScaner(applicationContext)
        bluetoothScanner.start(CoroutineScope(defaultDispatcher() + Job()))

        createNotification()
    }


    private fun createNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = "com.soterio.default_notification"
            val descriptionText = "Default Channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel("2837856", name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = applicationContext?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        val builder = applicationContext?.let {
            NotificationCompat.Builder(it, "2837856")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setColor(Color.BLUE)
                .setContentTitle("Monitor is scanning to keep you safe")
                .setContentText("If this notification disappears, please open the application")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        }

        with(applicationContext?.let { NotificationManagerCompat.from(it) }) {
            builder?.build()?.let {
                startForeground(1, it)
            }
        }

    }

}
