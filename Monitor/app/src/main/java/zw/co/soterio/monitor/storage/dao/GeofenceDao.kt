package zw.co.soterio.monitor.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import zw.co.soterio.monitor.storage.entity.GeofenceEntity

@Dao
interface GeofenceDao {
    @Query("SELECT * FROM geofences")
    fun getAllGeofences() : List<GeofenceEntity>

    @Query("DELETE FROM geofences")
    fun nukeGeofences()

    @Insert
    fun insertGeofence(geofenceEntity: GeofenceEntity)

    @Insert
    fun insertGeofences(vararg geofenceEntity: GeofenceEntity)

}