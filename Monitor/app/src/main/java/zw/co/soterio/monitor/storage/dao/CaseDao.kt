package zw.co.soterio.monitor.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import zw.co.soterio.monitor.storage.entity.Case

@Dao
interface CaseDao {
    @Query("SELECT * FROM cases")
    fun getAllCases() : List<Case>

    @Query("DELETE FROM cases")
    fun nukeCases()

    @Insert
    fun insertCases(case: Case)

    @Insert
    fun insertCases(vararg case: Case)
}