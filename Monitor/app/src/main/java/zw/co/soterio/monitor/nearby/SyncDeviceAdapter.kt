package zw.co.soterio.monitor.nearby

import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import com.google.android.gms.nearby.Nearby
import com.google.android.gms.nearby.connection.*
import com.google.gson.Gson
import timber.log.Timber
import zw.co.soterio.monitor.MonitorApplication
import zw.co.soterio.monitor.http.models.PacketJSON
import zw.co.soterio.monitor.storage.entity.Encounter


class SyncDeviceAdapter(context: Context) {
    private val mContext = context
    val STRATEGY: Strategy = Strategy.P2P_CLUSTER
    val SERVICE_ID = "120001"
    private var strendPointId: String? = null
    private var appUtils = MonitorApplication.appUtils

    fun startAdvertising() {
        Log.d("Nearby", "Starting advertising")
        val advertisingOptions = AdvertisingOptions.Builder().setStrategy(STRATEGY).build();
        getDeviceIdentifier()?.let { Nearby.getConnectionsClient(mContext).startAdvertising(it, SERVICE_ID, object: ConnectionLifecycleCallback() {
                override fun onConnectionResult(p0: String, connectionResolution: ConnectionResolution) {
                    when (connectionResolution.status.statusCode) {
                        ConnectionsStatusCodes.STATUS_OK -> {
                            Log.d("Nearby", "Connected")

                            strendPointId = p0
                            sendPayLoad(p0);
                        }
                        ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                            Log.d("Nearby", "Rejected")
                        }
                        ConnectionsStatusCodes.STATUS_ERROR -> {
                            Log.d("Nearby", "Error")
                        }
                    }
                }

                override fun onDisconnected(p0: String) {
                    strendPointId = null;
                }

                override fun onConnectionInitiated(endPointId: String, connectionInfo: ConnectionInfo) {
                    Nearby.getConnectionsClient(mContext).acceptConnection(endPointId, payloadCallback);
                    Log.d("Nearby", "Connection initiated")

                }
            }, advertisingOptions)
        };
    }

    private fun sendPayLoad(endPointID: String) {
        val gson = Gson()
        val packetJSON= PacketJSON(System.currentTimeMillis(), arrayListOf(Encounter(0,"",0,0.2,0.1,0,0)))
        gson.toJson(packetJSON)

        val bytesPayload = Payload.fromBytes(gson.toString().toByteArray())
        Nearby.getConnectionsClient(mContext).sendPayload(endPointID, bytesPayload)
            .addOnSuccessListener {
                Log.d("Nearby", "Sent")

            }.addOnFailureListener {
                Timber.d(it)
            }
    }

    fun startDiscovery() {
        val discoveryOptions = DiscoveryOptions.Builder().setStrategy(STRATEGY).build()
        Log.d("Nearby", "Starting discovery")
        Nearby.getConnectionsClient(mContext).startDiscovery(SERVICE_ID, object : EndpointDiscoveryCallback() {
            override fun onEndpointFound(@NonNull endpointId: String, @NonNull discoveredEndpointInfo: DiscoveredEndpointInfo) { getDeviceIdentifier()?.let {
                    Nearby.getConnectionsClient(mContext).requestConnection(it, endpointId, object : ConnectionLifecycleCallback() {
                        override fun onConnectionInitiated(@NonNull endpointId: String, @NonNull connectionInfo: ConnectionInfo) {
                            Nearby.getConnectionsClient(mContext).acceptConnection(endpointId, payloadCallback)
                            Log.d("Nearby", "Connection initiated")

                        }

                        override fun onConnectionResult(@NonNull s: String, @NonNull connectionResolution: ConnectionResolution) {
                            when (connectionResolution.status.statusCode) {
                                ConnectionsStatusCodes.STATUS_OK -> {
                                    Log.d("NearbyComm", "Connected to $s at ${discoveredEndpointInfo.serviceId}")
                                }
                                ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED -> {
                                    Log.d("NearbyComm", "Rejectec by $s at ${discoveredEndpointInfo.serviceId}")
                                }
                                ConnectionsStatusCodes.STATUS_ERROR -> {
                                    Log.d("NearbyComm", "Error on $s at ${discoveredEndpointInfo.serviceId}")
                                }
                                else -> {
                                }
                            }
                        }

                        override fun onDisconnected(@NonNull s: String) {
                            Log.d("NearbyComm", "Disconnected from $s at ${discoveredEndpointInfo.serviceId}")
                        }
                    })
                }
            }

            override fun onEndpointLost(@NonNull s: String) {
                // disconnected
            }
        }, discoveryOptions)
    }

    private fun getDeviceIdentifier(): String? {
        return appUtils!!.getDeviceIdentifier()
    }

    private fun onReceivePayLoad(endPointID: String, payload: Payload) {
        when(payload.type) {
            Payload.Type.BYTES -> {
                val gson = Gson()

                val secondaryDeviceLastUpdated = gson.fromJson(payload.asBytes()?.let { String(it) }, PacketJSON::class.java)
                if (secondaryDeviceLastUpdated != null) {
                    if(secondaryDeviceLastUpdated.Timestamp > appUtils?.getDatabaseLastUpdated()!!){
                        Log.d("SyncDevices", "Found timestamp $secondaryDeviceLastUpdated")

                    }
                }
            }
        }
    }

    private fun requestUpdate(endPointID: String) {

    }

    private fun sendDBUpdate(endPointID: String) {

    }

    private fun handleDBUpdate() {
        appUtils!!.writeDatabaseLastUpdated(System.currentTimeMillis())
    }

    private val payloadCallback = object: PayloadCallback() {

        override fun onPayloadReceived(s: String, payload: Payload) {
            onReceivePayLoad(s, payload)
        }

        override fun onPayloadTransferUpdate(s: String, payloadTransferUpdate: PayloadTransferUpdate) {
            if (payloadTransferUpdate.status == PayloadTransferUpdate.Status.SUCCESS) {
                Log.d("NearbyComm", "Payload recieved")
            }
        }

    }
}