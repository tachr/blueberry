package zw.co.soterio.monitor.utils

import android.content.Context
import android.util.Base64
import android.util.Log
import zw.co.soterio.monitor.MonitorApplication
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec


class CryptoUtils(context: Context) {
    private val random = SecureRandom()
    private val salt = ByteArray(256)
    private var keyBytes:ByteArray
    private var cipher: Cipher
    private var decipher: Cipher
    init {
        random.nextBytes(salt)
        val pbKeySpec = PBEKeySpec("vowtosecrecy".toCharArray(), salt, 1324, 256) // 1
        val secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1") // 2
        keyBytes = secretKeyFactory.generateSecret(pbKeySpec).encoded // 3
        val keySpec = SecretKeySpec(keyBytes, "AES") // 4
        val ivRandom = SecureRandom() //not caching previous seeded instance of SecureRandom
        val iv = ByteArray(16)
        ivRandom.nextBytes(iv)
        val ivSpec = IvParameterSpec(iv) // 2
        cipher = Cipher.getInstance("AES/CBC/PKCS7Padding") // 1
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec)
        decipher = Cipher.getInstance("AES/CBC/PKCS7Padding")
        decipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec)
    }


    public fun encryptPhone(phone:String) {
        val encrypted = cipher.doFinal(phone.toByteArray()) // 2
        MonitorApplication.appUtils!!.writeDeviceIdentifier(md5((String(encrypted))))
        Log.d("Encrypted", "Phone $phone ; Message ${md5(String(encrypted))}")
    }

    public fun decryptPhone(phone:String) {
        val encrypted = decipher.doFinal(phone.toByteArray()) // 2
        //MonitorApplication.appUtils!!.writeDeviceIdentifier(md5((String(encrypted))))
        Log.d("Encrypted", "Phone $phone ; Message ${md5(String(encrypted))}")
    }

    private fun md5(s: String): String {
        try {

            // Create MD5 Hash
            val digest = MessageDigest.getInstance("MD5")
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()

            // Create Hex String
            val hexString = StringBuffer()
            for (i in messageDigest.indices) hexString.append(
                Integer.toHexString(
                    0xFF and messageDigest[i].toInt()
                )
            )
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }


}