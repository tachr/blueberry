package zw.co.soterio.monitor.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import zw.co.soterio.monitor.storage.entity.Encounter

@Dao
interface EncounterDao{

    @Query ("SELECT * FROM Encounters")
    fun getAllEncounters():List<Encounter>

    @Query ("SELECT * FROM Encounters WHERE Identifier = :Identifier ORDER BY Timestamp DESC LIMIT 1")
    fun getEncounter(Identifier: String): Encounter

    @Query("SELECT * FROM Encounters WHERE Identifier = :Identifier ")
    fun getEncountersByIdentifier(Identifier: String): List<Encounter>

    @Query("DELETE FROM Encounters")
    fun nukeEncounters()

    @Insert
    fun insertEncounter(encounter: Encounter)

    @Insert()
    fun insertEncounters(vararg encounter: Encounter)

}