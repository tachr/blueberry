package zw.co.soterio.monitor.ble

import android.content.Context
import android.location.Location
import android.os.ParcelUuid
import android.util.Log
import androidx.room.RoomDatabase
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.polidea.rxandroidble2.LogConstants
import com.polidea.rxandroidble2.LogOptions
import com.polidea.rxandroidble2.RxBleClient
import com.polidea.rxandroidble2.RxBleConnection
import com.polidea.rxandroidble2.scan.ScanFilter
import com.polidea.rxandroidble2.scan.ScanResult
import com.polidea.rxandroidble2.scan.ScanSettings
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import kotlinx.coroutines.*
import timber.log.Timber
import zw.co.soterio.monitor.Constants
import zw.co.soterio.monitor.MonitorApplication
import zw.co.soterio.monitor.storage.DBInstance
import zw.co.soterio.monitor.storage.entity.Encounter


class RxBleScaner(val context: Context) {

    private val rxBleClient = RxBleClient.create(context)
    private var devices: MutableList<Pair<ScanResult, Int>> = mutableListOf()
    private val connections: MutableList<Disposable?> = mutableListOf()
    private val fusedLocationClient: FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context)
    private lateinit var appDatabase: RoomDatabase

    private val monitorFilter = ScanFilter.Builder()
        .setServiceUuid(ParcelUuid(Constants.MONITOR_SERVICE_UUID))
        .build()

    private var scanDisposable: Disposable? = null
    private var scanJob: Job? = null

    /*
     When iPhone goes into the background iOS changes how services are advertised:

       1) The service uuid is now null
       2) The information to identify the service is encoded into the manufacturing data in a
       unspecified/undocumented way.

     */


    private val settings = ScanSettings.Builder()
        .setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
        .build()

    @InternalCoroutinesApi
    fun start(coroutineScope: CoroutineScope) {
        appDatabase = MonitorApplication.database!!
        Log.d("RxBle", "Starting Scan")
        val logOptions = LogOptions.Builder()
            .setLogLevel(LogConstants.DEBUG)
            .setLogger { level, tag, msg -> Timber.tag(tag).log(level, msg) }
            .build()
        RxBleClient.updateLogOptions(logOptions)
        scanJob = coroutineScope.launch {
            while (isActive) {
                Timber.d("scan - Starting")
                scanDisposable = scan()
                var attempts = 0
                while (attempts++ < 10 && devices.isEmpty()) {
                    if (!isActive) return@launch
                    delay(5000)
                }
                Timber.d("scan - Stopping")
                if (!isActive) return@launch

                scanDisposable?.dispose()
                scanDisposable = null

                // Some devices are unable to connect while a scan is running
                // or just after it finished
                delay(1_000)

                devices.distinctBy { it.first.bleDevice }.map {
                    Timber.d("scan - Connecting to $it")
                    connectToDevice(it.first, it.second, coroutineScope)
                }

                connections.map { it?.dispose() }
                connections.clear()
                devices.clear()
            }
        }
    }

    private fun scan(): Disposable? = rxBleClient
            .scanBleDevices(
                settings,
                monitorFilter
            )
            .subscribe(
                {
                    Log.d("RxBle", "Scan found = ${it.bleDevice}")
                    devices.add(Pair(it, it.scanRecord.txPowerLevel))
                },
                ::onConnectionError
            )

    fun stop() {
        scanDisposable?.dispose()
        scanDisposable = null
        scanJob?.cancel()
    }

    private fun connectToDevice(scanResult: ScanResult, txPowerAdvertised: Int, coroutineScope: CoroutineScope) {
        val macAddress = scanResult.bleDevice.macAddress
        Log.d("RxBle", "Found $macAddress mmeta ${scanResult.scanRecord.serviceData}")
        Timber.d("Connecting to $macAddress")
        scanResult
            .bleDevice
            .establishConnection(false)
            .flatMapSingle { connection ->
                negotiateMTU(connection)
            }
            .flatMapSingle { connection ->
                read(connection, txPowerAdvertised, coroutineScope)
            }
            .doOnSubscribe {
                connections.add(it)
            }
            .take(1)
            .blockingSubscribe(
                { event ->
                    storeEvent(event)
                },
                { e ->
                    Timber.e("failed reading from $macAddress - $e")
                }
            )
    }

    private fun negotiateMTU(connection: RxBleConnection): Single<RxBleConnection> {
        // the overhead appears to be 2 bytes
        return connection.requestMtu(2 + 410)
            .doOnSubscribe { Timber.i("Negotiating MTU started") }
            .doOnError { e: Throwable? ->
                Timber.e("Failed to negotiate MTU: $e")
                Observable.error<Throwable?>(e)
            }
            .doOnSuccess { Timber.i("Negotiated MTU: $it") }
            .ignoreElement()
            .andThen(Single.just(connection))
    }

    private fun read(connection: RxBleConnection, txPower: Int, scope: CoroutineScope): Single<Event> = Single.zip(
            connection.readCharacteristic(Constants.MONITOR_IDENTITY_CHARACTERISTIC_UUID),
            connection.readRssi(),
            BiFunction<ByteArray, Int, Event> { characteristicValue, rssi ->
                Event(characteristicValue, rssi, txPower, scope, System.currentTimeMillis())
            }
        )

    private fun onConnectionError(e: Throwable) {
        Timber.e("Connection failed with: $e")
    }

    private fun storeEvent(event: Event) {
        Timber.d("Event $event")
        createOrUpdateContactEvent(
            event.identifier,
            event.rssi,
            event.timestamp,
            event.txPower
        )
    }

    private fun createOrUpdateContactEvent(identifier: ByteArray, rssi: Int, timestamp: Long, txPower: Int) {
        val deviceIdentifier = String(identifier)

        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                if(location !=null) {
                    val encounter = Encounter(0, deviceIdentifier, timestamp, location.latitude, location.longitude, rssi, txPower)
                    saveEncounter(encounter)

                } else {
                    val encounter = Encounter(0, deviceIdentifier, timestamp, 0.00, 0.00, rssi, txPower)
                    saveEncounter(encounter)

                }
            }
            .addOnFailureListener {
                val encounter = Encounter(0, deviceIdentifier, timestamp, 0.00, 0.00, rssi, txPower)
                saveEncounter(encounter)
            }
    }

    private fun saveEncounter(encounter: Encounter) {
        val thread: Thread = object : Thread() {
            override fun run() {
                (appDatabase as DBInstance).encounterDao().insertEncounter(encounter)
                Log.d("RxBle", "Saved Encounter $encounter")
            }
        }
        thread.start()
    }

    @Suppress("ArrayInDataClass")
    private data class Event(
        val identifier: ByteArray,
        val rssi: Int,
        val txPower: Int,
        val scope: CoroutineScope,
        val timestamp: Long
    )


}

