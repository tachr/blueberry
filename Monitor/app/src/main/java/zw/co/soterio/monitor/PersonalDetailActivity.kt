package zw.co.soterio.monitor

import android.app.ActivityOptions
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.transition.Explode
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import java.text.SimpleDateFormat
import java.util.*


class PersonalDetailActivity : AppCompatActivity() {

    private lateinit var btnNext: MaterialButton
    val myCalendar: Calendar = Calendar.getInstance()
    private lateinit var dateOfBirth: TextInputLayout
    private lateinit var deDOfBirth: TextInputEditText
    private lateinit var firstName: TextInputLayout
    private lateinit var surname: TextInputLayout
    private lateinit var physicalAddress: TextInputLayout



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboard)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        dateOfBirth = findViewById(R.id.dateOfBirth)
        firstName = findViewById(R.id.firstName)
        surname = findViewById(R.id.surname)
        physicalAddress = findViewById(R.id.physicalAddress)



        deDOfBirth = findViewById(R.id.deDOfBirth)
        val date = OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                val myFormat = "MM/dd/yyyy" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)
                dateOfBirth.editText!!.setText(sdf.format(myCalendar.time))
            }

        deDOfBirth.setOnClickListener {
            // TODO Auto-generated method stub
            DatePickerDialog(this, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH], myCalendar[Calendar.DAY_OF_MONTH]).show()
        }

        dateOfBirth.setOnClickListener {
            // TODO Auto-generated method stub
            DatePickerDialog(
                applicationContext, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            ).show()
        }


        btnNext = findViewById(R.id.btnContinuePhone)
        btnNext.setOnClickListener {
            when {
                dateOfBirth.editText!!.text.toString() == "" -> {
                    dateOfBirth.error = "Date of Birth is required"
                }
                firstName.editText!!.text.toString() == "" -> {
                    firstName.error = "First name is required"
                }
                surname.editText!!.text.toString() == "" -> {
                    surname.error = "Surname is required"
                }
                physicalAddress.editText!!.text.toString() == "" -> {
                    physicalAddress.error = "Physical address is required"
                }
                else -> {
                    MonitorApplication.appUtils!!.setUserFirstName(firstName.editText!!.text.toString())
                    MonitorApplication.appUtils!!.setUserSurname(surname.editText!!.text.toString())
                    MonitorApplication.appUtils!!.setUserAddress(physicalAddress.editText!!.text.toString())
                    MonitorApplication.appUtils!!.setUserBirthday(dateOfBirth.editText!!.text.toString())

                    startActivity(Intent(this, MobilePhoneActivity::class.java), ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
                }
            }
        }

    }

}
