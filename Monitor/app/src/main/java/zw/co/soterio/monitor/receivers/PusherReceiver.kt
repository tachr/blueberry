package zw.co.soterio.monitor.receivers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.pusher.client.Pusher
import com.pusher.client.PusherOptions
import com.pusher.client.connection.ConnectionEventListener
import com.pusher.client.connection.ConnectionState
import com.pusher.client.connection.ConnectionStateChange
import org.json.JSONObject
import zw.co.soterio.monitor.MonitorApplication
import zw.co.soterio.monitor.R
import zw.co.soterio.monitor.storage.DBInstance
import zw.co.soterio.monitor.storage.entity.Case
import zw.co.soterio.monitor.storage.entity.GeofenceEntity

class PusherReceiver(mContext:Context) {
    val context = mContext
    lateinit var localBroadcastManager: LocalBroadcastManager;


    fun startReceiver() {

        Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable ->
            Log.e(
                "Error" + Thread.currentThread().stackTrace[2],
                paramThrowable.localizedMessage
            )
        }

        localBroadcastManager = LocalBroadcastManager.getInstance(context)
        val positiveFiler = IntentFilter("POSITIVE_MATCH")
        localBroadcastManager.registerReceiver(positiveMatchReceiver, positiveFiler)
        //TODO("Not yet implemented")

        val options = PusherOptions()
        options.setCluster("ap1");

        val pusher = Pusher("9588771f1f952adef79b", options)

        pusher.connect(object : ConnectionEventListener {
            override fun onConnectionStateChange(change: ConnectionStateChange) {
                Log.i("Pusher", "State changed from ${change.previousState} to ${change.currentState}")
            }

            override fun onError(
                message: String?,
                code: String?,
                e: Exception?
            ) {
                Log.i("Pusher", "There was a problem connecting! code ($code), message ($message), exception($e)")
            }
        }, ConnectionState.ALL)

        val thread: Thread = object : Thread() {
            override fun run() {
                try {
                    val channel = pusher.subscribe("cases")
                    channel.bind("new-case") { event ->
                        Log.i("Pusher","Received event with data: ${event.data}")

                        val jsonObject = JSONObject(event.data)
                        Log.d("Sync", "${jsonObject.getJSONObject("message")}")
                        val case: Case = Gson().fromJson(jsonObject.getJSONObject("message").toString(), Case::class.java)

                        val thread: Thread = object : Thread() {
                            override fun run() {
                                MonitorApplication.database!!.caseDao().insertCases(case)
                                val localIntent = Intent("CASES_UPDATED")
                                localBroadcastManager.sendBroadcast(localIntent)
                                val cases = MonitorApplication.database!!.caseDao().getAllCases()
                                for (c in cases) {
                                    if(MonitorApplication.database!!.encounterDao().getEncountersByIdentifier(c.Identifier).isNotEmpty()) {
                                        Log.d("Receiver", "You might have been exposed")
                                        showPositiveNoti(context)
                                    } else {
                                        //Log.d("Sync", "Found ${database!!.encounterDao().getAllEncounters()[0]}")
                                    }
                                }
                            }
                        }

                        val channel2 = pusher.subscribe("geofences")
                        channel2.bind("new-geofence") { event ->
                            Log.i("Pusher","Received event with data: ${event.data}")

                            val jsonObject = JSONObject(event.data)
                            Log.d("Sync", "${jsonObject.getJSONObject("message")}")
                            val geofence: GeofenceEntity = Gson().fromJson(jsonObject.getJSONObject("message").toString(), GeofenceEntity::class.java)

                            MonitorApplication.database!!.geofenceDao().insertGeofence(geofence)
                            val localIntent = Intent("GEOFENCES_UPDATED")
                            localBroadcastManager.sendBroadcast(localIntent)

                        }


                    }
                } catch(e:Throwable) {

                }
            }
        }
        thread.start()

    }

    private val positiveMatchReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val thread: Thread = object : Thread() {
                override fun run() {
                    val cases = MonitorApplication.database!!.caseDao().getAllCases()
                    for (c in cases) {
                        if(MonitorApplication.database!!.encounterDao().getEncountersByIdentifier(c.Identifier).isNotEmpty()) {
                            Log.d("Receiver", "You might have been exposed")
                            showPositiveNoti(context)
                        } else {
                            //Log.d("Sync", "Found ${database!!.encounterDao().getAllEncounters()[0]}")
                        }
                    }
                }
            }
            thread.start()

        }

    }

    private fun showPositiveNoti(context: Context?) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = "com.soterio.positivi_notification"
            val descriptionText = "Positive Channel"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val mChannel = NotificationChannel("2876733973", name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }

        val builder = context?.let {
            NotificationCompat.Builder(it, "2876733973")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setColor(Color.RED)
                .setContentTitle("Exposure Alert")
                .setContentText("You might have been exposed to COVID-19")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
        }

        with(context?.let { NotificationManagerCompat.from(it) }) {
            builder?.build()?.let { this?.notify(1, it) }
        }



    }

}