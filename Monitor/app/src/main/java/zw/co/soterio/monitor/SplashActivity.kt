package zw.co.soterio.monitor

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import zw.co.soterio.monitor.utils.CryptoUtils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        //CryptoUtils(this).decryptPhone("24c5ef6fbd9bea5ac33014917827728")

        if(MonitorApplication.appUtils!!.getShouldOnBoard()!!) {
            val background: Thread = object : Thread() {
                override fun run() {
                    try {
                        sleep(2 * 1000.toLong())
                        startActivity(Intent(applicationContext, PersonalDetailActivity::class.java))
                        //Remove activity
                        finish()
                    } catch (e: Exception) {
                    }
                }
            }
            // start thread
            // start thread
            background.start()

        } else {
            val background: Thread = object : Thread() {
                override fun run() {
                    try {
                        sleep(2 * 1000.toLong())
                        startActivity(Intent(applicationContext, MainActivity::class.java))
                        //Remove activity
                        finish()
                    } catch (e: Exception) {
                    }
                }
            }
            // start thread
            // start thread
            background.start()
        }
    }
}
