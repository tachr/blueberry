# Soterio Monitor: A distributed peer-to-peer contact tracing and awareness network


Medicinal and Safety Solutions, Guava Initiative\
Team Soterio

Version 0.1 Apr 30th, 2020

*The process of manual contact tracing, if applied aggressively, can slow the spread of the disease. This is true in spite of the obvious limitations of manual contact tracing: people don’t remember who they have had contact with many days ago, and even if they do remember them they may not know their names or how to contact them. [PACT Protocol -Weast Coast Pact - MIT]()*

*It has to be noted that in the early phases, the emphasis is on rapid iteration and deployment of solutions for epidemic tracking. In the later phases, the goal is building encrypted computational methods that can be useful in any future societal disruptions.*

## ABSTRACT
The coronavirus epidemic of 2019 is easily the worst of all pandemics mankind has ever seen, the interconnectedness of the world speeding up its spread. Countries across the globe have come up with many different strategies to fight and contain the virus. From the basket of strategies lockdowns have become the way to go for most countries despite their negative impact on economies. In due time this impact has now begun to be felt by countries like Zimbabwe prompting to the loosening of lockdown conditions and come up with different strategies to contain the virus in order to avoid an irrecoverable economic disaster. Following the release of the BlueTrace protocol by the Government of Singapore in April, we have looked into how we can implement a protocol similar to that but efficient and relevant in the context of Zimbabwe.

*The goal of contact tracing is to quickly identify individuals who may be virus carriers, before they even show symptoms, so that they may be tested, quarantined, and/or advised to monitor themselves for symptoms. By removing such individuals from the circulating population, the spread of the virus may be diminished.*


## CONTEXT
Qouting the Bluetrace Whitepaper, "Contact tracing is an important tool for reducing the spread of infectious diseases. Its goal is to reduce a disease’s effective reproductive number (R) by identifying people who have been exposed to the virus through an infected person and contacting them to provide early detection, tailored guidance, and timely treatment. By stopping virus transmission chains, contact tracing helps “flatten the curve” and reduces the peak burden of a disease on the healthcare system.". Contact tracing could allow the government of Zimbabwe to efficiently track the transmission chain from a person who has tested positive to the virus down to all the people they may have came in contact with and visualize the virus' heatmaps. Soterio Monitor aims to reduce the strain on resources put in contact tracing by helping the workforce in the medical field with waypoints to people who need testing and reducing the chances of false positives and false negatives that arise from applications relying solely on Bluetooth.That being said, Soterio Monitor does not replace the need for human contact tracers and trackers but will only supplement to their work making it more economic and efficient.

*The goal  is not to subsume medical professionals, but to give them the tools that they can use in their fight with disease. It is up to medical authorities to decide what to advise individuals to do based on the information they receive via the app. Many factors may come into play in determining whether or not Alice should be tested for disease -- her symptoms, her risk factors, her location -- her exposure risk score. Any decisions for what Alice should do from here
should be done in consultation with doctors and health authorities.*


*Note: Soterio monitor draws most of its guidelines from BlueTrace, PACT protocol, GAE and TCN protocol but does not implement them in isolation, rather it uses a combination of the protocols, geofencing technology ([Geospark](https://geospark.co/)), [Hype-SDK](https://hypelabs.io) and the Bridgefy SDK to enable transfer of information in phones with no data connection. The implementations were revisited to address the case of Zimbabwe and countries with a low percentage of people with consistent active internet connection*

#### Why we chose to log GPS data locally
*Contextual information is key: Users do not like to get an alert without understanding the context. They need to now where exactly the encounter took place in order to trust the system. This location information can be provided by GPS location logging. Access to location information can also help human contact tracers conduct interviews by helping the patients, for example, remember if they were wearing a mask or not, whether they shook hands or not, etc. This location data just like the log of encounters, will never leave a users phone*

## OVERVIEW
Soterio Monitor logs encounters between two cellphones with the application installed to facilitate contact tracing. When two participating devices encounter each other, they exchange non-personally identifiable messages that contain an identifier, time when the phone's local database of identifiers that tested positive to the virus was last updated. The time when the phone's local database was last updated enables phones to share updates from the central database in a peer-to-peer network by allowing the phone with the outdated database to request an update from the phone with the newer. The request will trigger the phone with the newer database to send its database of positive identifiers and red zones, but not its log of encounters to the other phone. After recieving the token from another phone, the phone will log the current GPS to a local database using the recieved identifier as a primary key. This is crucial to providing context of contact to prevent false positives and false negatives by allowing users who had a phone at the given time and location notify other people whom they were with but did not have phones. Ultimately this reduces the requirement for mass adoption for the application to be effective and also allows the health organization to be able to create infection heat maps.



#### WHEN A USER TESTS POSITIVE TO THE VIRUS
When a user is infected, their ID is added to the central database of positive ID's and will be distributed to other in a similar peer-to-peer fashion. This enables all phones to keep an updated database of positive identifiers without the need for a consistent internet connection. Also when a user tests positive, the medical officials will decrypt the local database of encounters in the user's phone and contact the people they have came in contact with in the past 21 days.

Only the health authority has the ability to decrypt the local database of encounter history to obtain and use personally-identifiable information to filter for close contacts and contact potentially infected users. The health authority then filters for people at risk based on the disease’s epidemiological parameters especially time of exposure (measured by the length of a continuous cluster of encounters).

Generally the contact tracing process involves an interview with the patient, where the patients are asked to recall where they have been and who they have been in contact with recently. This information is used together with the data to adjust the proximity and duration filtering thresholds based on the patient-reported location and context. The health authority then contacts individuals assessed to have a high likelihood of exposure to the disease, to provide medical guidance and care. Note that according to the BlueTrace implementation this workflow can be automated, however, it is not recommened, and have therefore not implemented it in Soterio Monitor.
 
#### GEOFENCING
The health authority will also create geofences of areas that are considered highly contagious and the application will give you a notification when you're about to enter such a geofence. Updates about these geofences will also be shared in the same peer-to-peer network. This will allow users to personally protect themselves by avoiding such areas or taking measures to safeguard their health in such areas.


#### BROADCASTING AWARENESS INFORMATION
In addition to contact tracing, the peer-to-peer network will allow the government to brooadcast awareness messages and information about red-zones to the public at almost zero-cost. The public will recieve the message whether or not they have internet access, a sim-card or their phone is in airplane mode.

## WHY NOT JUST BLUETOOTH
Exposure notification according to BlueTrace only reports the day of exposure and duration of exposure but not the location or time. We believe the context of location and time is critical for 
- (i) user to self assess if they were exposed (e.g. if they were wearing a mask or maintaining social distance at that time) and inform others who were with this person in case they were not carrying the smartphone 
- (ii) improve user’s trust in the system to reject false positives (e.g. if they picked up a signal from behind a wall)   
- (iii) help public health officials to perform the contact tracing operations that require such a context (e.g. to request everyone at a wedding to self-isolate at home if there was an infected person for a long time). Lack of context and the lack of agency can lead to irrational behavior and even civil unrest.

Our goal is to allow location-time context to be delivered to the user during exposure notification. This will be achieved by logging location at time of contact, in the app but not sharing it during token exchange: The App stores time and location along with the packets and will use the identifier as a primary key to find these locations.

We are based on the idea that GPS information will be available to a healthy phone only if exposed. If Alice is healthy and comes in proximity to Bob who was later diagnosed Covid+, Alice will be able to see the location (and time) of that encounter but the rest of her location history remains invisible or encrypted. 

The idea is to use BLE for proximity and GPS for context (but not proximity).

#### Adoption rate vs effectiveness
Bluetooth requires many people to use the apps for it to be valuable. In Singapore the Bluetooth App penetration is 12% which means only 1.44% of encounters are recorded (0.12*0.12). As a reference, Casey Newton has a good piece on Why Bluetooth apps are bad at discovering new cases of COVID-19. While adoption can be increased through government and big tech buy-in, these numbers still hamper effectiveness.

#### GPS based App scales linearly 
With a 12% adoption rate, 12% of infected hotspots will be identified and nearly everyone in town will hear about them, eg. local news channels and through the app's peer-to-peer network. GPS is useful even if smartphone penetration in a region is not widespread.

#### Health Organisations Perspective: Public health needs more than just encounters
The prime need of the health officials is their ability to call the exposed person directly to assess their personal situation and provide guidance about testing/isolation/hospitalization. Health care officials also need dashboards of emerging hotspots to be able to map the spread. Without GPS, it is difficult to create tools for heatmaps and spread analysis. Bluetooth does have certain advantages especially in dense urban environments, but the optimal contact tracing solution will ultimately be multimodal (Bluetooth, GPS, and Wi-Fi), leveraging the advantages of each technology. For example, contextual information from GPS location logs can help a simple rejection mechanism for the false positives created by Bluetooth. GPS also allows for the creation of crucial virus heatmaps for health professionals, without needing large scale adoption by the population. And self-reporting (heatmap as well as social circles) is very important as going to testing sites still has a lot of friction, especially among young people. While a Bluetooth API certainly makes aspects of contact tracing easier, public health officials need more than simply contact tracing to address the epidemic. They need a larger ecosystem that can help them with quarantine management, health verification, patient interview, hotspot identifying, and more.

## USERS
#### Health Organization
They are responsible for maintaining the database testing and confirming that a person has tested positive before their Identifier is added to the database of positive Identifiers. They are also responsible for the decryption of the log of encounters from a user to perform analysis basing on the tracing conduct and filter out the people who are at a higher risk of contraction and contact them. The organization creates and manages red zones (virus hotspots) and creates models from the contextual data that is given by users. It has to be noted that contextual data is provided anonymously therefore all analysis performed on the data will be in one way or the other, aggregated.

#### Consumers
These are the regular citizens of a country who will install the application on their cellphones, or be around someone with the app installed. It is not a requirement that everyone installs the application but a considerable majority must, in-order for the health organization to derive any meaningful information from the data provided to them.

## DATA PROTECTION AND PRIVACY
- Limited collection of personally-identifiable information. The only personally-identifiable information collected is a phone number, which is encrypted end-to-end by the phone and only the health authority can decrypt it.

- Local storage of encounter history. Each user’s encounter history is stored exclusively on their own device. The health authority only has access to this history when a person tests positive and they upload it.
- Local storage of encounter history. The health authority only has access to this GPS data when a person tests positive and they upload it.

The implementation is built with respect to the document that describes Temporary Contact Numbers, a decentralized, privacy-first contact tracing protocol developed by the TCN Coalition. This protocol is built to be extensible, with the goal of providing interoperability between contact tracing applications. The TCN protocol and related efforts are designed with the Contact Tracing Bill of Rights in mind.

Consumers are explained to, in detailed, the nature of the data being collected, how it is used, the impacts of the disclosure of their data and have the freedom to opt-out at any time. 

Data collection is limited to geographical and demographic data without the identifiers .i.e data collected from any particular device can never be linked back to or used to re-identify individuals, even by entities legally allowed to perform such linkage. This is inline with the Contact Tracing Bill of Rights, only collecting the minimum data achieve a particular goal. 

Only the contextual data is stored past the timeline described by the epidemiological standard of the virus. A positively tested identifier is retained any longer than is required to conduct effective contact tracing. The reason for retaining contextual data is that it allows the entity to create epidemiological models and make informed decisions in the future basing on past and present data. This can be achieved without putting people's privacy at risk as their identity cannot be inferred from the contextual data. 



#### Author
Tatenda Christopher Chinyamakobvu - (Chinhoyi University of Technology)\
Phone: +263714060057\
WhatsApp: +263788179114

#### Contributors
Munyaradzi Gordon Muneka\
Elvin Kakomo\
Sean Muchenje


#### *References*
[ Location Context in Apple/Google Bluetooth API for Informed Decision Making
](https://docs.google.com/document/d/1uTjdUetEEtnwN-6_iw3HTZOdAd0kKsK7GR1YbdS10Ss/edit)

[Multi-Hop Analyisis, Bluetooth Proximity, Pandemics and Others](https://www.researchgate.net/publication/340789081_Bluetooth_based_Proximity_Multi-hop_Analysis_and_Bi-directional_Trust_Epidemics_and_More)

[MIT Safepaths](http://safepaths.mit.edu/)

[God's Eye View: Will global AI empower us or destroy us?](https://www.ted.com/talks/ramesh_raskar_god_s_eye_view_will_global_ai_empower_us_or_destroy_us)


