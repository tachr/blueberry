#### The repository has been made private. Contact Chris if someone needs to be added to the repo

## Study Materials
- [Bluetooth LE basics](https://www.youtube.com/watch?v=qx55Sa8UZAQ)

## RULES
One rule to rule them all
#### *Pull then branch and do your work then commit to your branch and test before merging with master*

## DIR
- #### Monitor
    - Has the source code for Android in kotlin. Just open it in Android Studio and let it sync. I have not yet set up firebase so it should work just fine in the studio
    - Minimum API is set to 21 (Android 5) and target API is 29
- #### dashboard
    - This is the server backend code with the dashboard also. Being implemented in NodeJS (Express and NextJS - React). Took time deciding how to best implement it, and finally decided to implement a salted MVC. The pages directory contains the source for the View in React, also using tailwind CSS as the styling frame.\
    To start working on it. Open command prompt/terminal and use *npm* or *yarn* 
            
            npm install
            yarn install  