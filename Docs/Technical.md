# SOTERIO MONITOR TECHNICAL OVERVIEW

### *This paper is work in progress and is subject to continous review*

Mankind has seen a great deal of innovation since the dawn of the Internet in the early 60s, with the invention of packet switching and the early days of the ARPANET. Since then, the Web has seen tremendous growth, reaching its first billion users in less than a decade and nearly half the world’s population to this day, only a little over 25 years after its birth. But nearly half the world’s population being connected means there’s another half that is not. At the same time, the Internet’s growth is already showing signs of slowing down. The industry is trying to revert this downwards trend by thinking out of the box about reachability, such as Google’s Project Loon, or Facebook’s internet.org[5], ultimately creating a shift on how we are bringing connectivity to the world. This shift to bringing connectivity to devices without internet could play a pivotal role in increasing the reach and effectiveness of contact racing applications.

## CONCEPT OVERVIEW

#### Entities
- Users of the phone app. Individuals using the application may either be diagnosed by a
health provider as COVID-positive or non-diagnosed; this status can change over time.
- Health providers are authorized to make a positive diagnosis that an individual has
COVID-19, based on a positive test or an evaluation of symptoms.
- Testing authorities who ensure that only information from diagnosed individuals can be
uploaded.
- An exposure database storing information uploaded by diagnosed individuals. For
efficiency reasons, the exposure database may be sharded by geographical region.

#### Responsibilities
- The server mantains the "exposure database", adding newly diagnosed patients' identifiers in the database and decrypting their local encounter to assess who else is at risk. The server will also be used to create geofences based on the user's location history, these will be virus' heatmaps which will be used to make informed decisions by the Health organizations. It can be on-premise or  in the cloud. Push notifications will be used to alert those who will have been in contact with the patient in the previous 21 days and to spread awareness messages. The same server will also mantain a database of overall COVID statistics that should be updated regularly. 
  - The geoofences are be implemented using, preferably Geospark as it is the same SDK being used to obtain the locations with considerable accuracy even when the device is offline.  

- The applications create a decentralized peer-to-peer network allowing updates from the database to reach all devices including those without internet connection. The same network will be used to spread awareness messages to applications without internet connection at relatively zero cost.
   - The peer-to-peer network is a hybrid mesh network sharing messages that are either locked or not locked to a particular identifier. In the case of a locked message, all devices in the network will forward the message and delete but not read it until it reaches the destination. This will allow us to send custom push notifications to particular identifiers.
   - Mesh networks contrast with standard networks in topology. Instead of relying on fixed infrastructure, devices are capable of relaying data for each other and can operate as routers. When two devices are communicating, intermediary proxies forward content for them. Without a centralized entity managing the network, such as an access point, the network is not sensitive to single point of failure. It’s notable that the medium is variable; that is, mesh networks can be wired or wireless, over Wi-Fi, Bluetooth, or anything else, even combined. Such networks can also be static or dynamic, depending on whether the nodes are mobile or not. But mesh networks are not meant for replacing existing frameworks, but rather to complement them. Therefore if a device has internet connection, it will prefer using that for recieving updates over the mesh network.
   - The mesh network is made possible by implementing the Hype SDK and Bridgefy SDK
   - All instances of the application will operate in broadcast and recieving mode simultaneously.


## REGISTRATION AND TOKENS
When the user of the Soterio Monitor app registers with their phone number, the phone encrypts the user’s phone number with a public key to generate an identifier. Phone numbers are the only personally-identifiable information required from the user. The phone numbers are used to contact users if they are found to have had prolonged exposure to an infected person. Only the health authority holds the private key to decrypt the identifiers and only the phone holds the app holds the public key to encrypt the identifiers.

#### Encounter Message
The Encounter Message is a UTF-8 encoded JSON. The fields in the JSON are as follows

        {
            "identifier": PhoneNumberEncryptedWithRotatingPublicKey,
            "lastUpdated": "149950593020",
            "model": "Samsung Galaxy S6",
        }

These packets are stored on a local database and encrypted by the phone and are never shared with other phones in the network. Only the phone and the server can decrypt the database of encounters. On the phone it is to check with the database of positive identifiers if they have not come in contact with any, and on the server it is to be able to extract the log of people who have came in contact with an infected person. 


The Encrypt method could be AES (key size can be adjusted to use standard 128 or 256 bit size).

- Phone Y emits [lastUpdated,  identifier]
- Phone X, over BT, receive this [lastUpdated, identifier] and stores identifier locally.
- Phone X gets current GPS and stores it in a local database with identifier as primary key
- Once diagnosed as infected, phone Y uploads identifier database  
- Phone X, downloads "exposure database".
- Phone X looks up local encounter database and checks for matches from the exposure database
- If match is found, it will use the identifier as key to obtain GPS from the local GPS database and decrypt it


The template of the positive identifiers database will be

        {
            "identifier": "9d45tg6675h3392j2jskbxbshw2134",
            "timestamp": "159006949392",
        }
        

## EXPOSURE DATABASE


## NETWORKING

#### According to the HYPE SDK documentation
The concept of mesh networking is probably better understood by comparison with other well-known
networking topologies. Consider a Local Area Network (LAN) where devices are connected to a central
access point. This is called a star topology, and is
unarguably the most common network topology, as is
easily recognizable by the representation in the figure
below, on the left. This contrasts with the illustration
on the right, where devices instead connect directly,
eliminating the need for a central entity. Notice that
all nodes are still somehow reachable. In this case,
devices can act as routers and forward traffic for others, enabling the content to hop between them until it
reaches a destination. But networks may still be hybrid. If infrastructure is available, mesh networks can
take advantage of it and in many cases improve their
functionality.

When delivering content over a network, the choice
of path bears consequences. Specifically, choosing
the best path is a function of multiple variables: overhead, congestion, stability, battery, etc. The SDK provides several API entry points for sending data. It
might be noticeable that these lack parameters for
specifying which transport to use, but there’s a reason
for that. Choosing the transport when sending data is
a common request from developers, but
situations exist under which the feature could be misleading. This happens in mesh. Consider a multi-hop
route between two devices. It might seem tempting
for a device to use its fastest transport, say, Infrastructure Wi-Fi, to reach another device, even if a Bluetooth alternative exists. The thing is, on a multi-hop
scenario the rest of the path is not known, and it may
contain bottlenecks, greatly reducing the path’s viability. In fact, making a choice of best path using the
transport as criteria is not possible without knowledge of the topology ahead. Rather, the SDK scores the
connections, allowing it to choose the best according
to a probabilistic measure of performance

In proactive routing nodes periodically exchange
topological information, notifying each other of what
other devices are reachable on the network. This information is exchanged through control packets, a
specific type of packet that nodes use to flood the
network with topological metadata. This process
enables nodes to keep up-to-date routing tables of
paths to other reachable nodes, which is why such
protocols are often also called table driven. Such information also enables the network to quickly heal
from link breakage, by relying on the knowledge of
alternative paths to the same destination. Depending on the type of protocol, such tables usually keep
next hop information along with other path metadata
attributes, such as cost and configuration. For a specific node, the set of participants that it is proactively
aware of is called its proactive area. The advantage of a proactive
approach is that optimal paths to nodes on this set
are known, and thus communication links are immediately available when needed, raising the probability of an update reaching a device at the peripheral.

The implementation uses several techniques to optimize the network, the first thing to understand is how devices are identified on the
network. It uses three components for that: an
app identifier, a device identifier, and a user identifier.
The app identifier is used to segregate the network, this identifier
is always the same. Device identifiers are generated
and managed by the implementation, and used to uniquely identify
devices. Although these two identifiers, put together,
are already universally unique, the app also sets the user identifiers, which are the ones that are the encrypted phone number. All three are collectively used for discovery and routing purposes.

## SECURITY
The first step in securing communications is to establish encrypted end-to-end links between two devices. The SDK implements the Diffie-Hellman, allowing any two
peers to safely exchange data without the worry that
attackers may read or tamper with the contents. This
is true even in mesh. This method is used to encrypt
the data in a way that only the two end devices can
interpret. Plus, the packet is validated at the destination, meaning that if attackers attempt to tamper
with the data, the receiver will be aware of it and will not save the packet in the encounter log.


#### Author
Tatenda Christopher Chinyamakobvu - *Chinhoyi University of Technology*\
Phone: +263788179114\
WhatsApp: +263714060057

#### *References*
- [Dr. Lawrence G. Roberts (IEEE). The Evolution of Packet Switching.
](http://www.packet.cc/files/ev-packet-sw.html )
- [Felix Richter (statista). The Not So World Wide Web](https://www.statista.com/chart/3512/internet-adoption-in-2015/)
- [Davey Alba (Wired). Mary Meeker: The Internet’s Growth Is Actually
Slowing Down](https://www.wired.com/2015/05/20-years-mary-meeker-says-internet-growth-slowing/) 
- [X. Project Loon]( https://x.company/loon/ )
- [facebook.internet.org](https://info.internet.org/en/) 

- [ Location Context in Apple/Google Bluetooth API for Informed Decision Making
](https://docs.google.com/document/d/1uTjdUetEEtnwN-6_iw3HTZOdAd0kKsK7GR1YbdS10Ss/edit)
